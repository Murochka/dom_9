/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ua.com.musiienko.dom_9;

/**
 *
 * @author dennisyarmosh
 */
public class MyCalculator {
    
    public float add(float a, float b) {
        return a+b;
    }
    
    public float minus(float a, float b) {
        return a - b;
    }
    
    public float divide(float a, float b) {
        if (b == 0) {
            throw new ArithmeticException("division by Zero");
        }
        return a / b;
    }
    
    public float multiply(float a, float b) {
        return a * b;
    }
    
}
