/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ua.com.musiienko.dom_9;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dennisyarmosh
 */
public class MyCalculatorTest {
    
    public MyCalculatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class MyCalculator.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        MyCalculator instance = new MyCalculator();
        
        float a = 3;
        float b = 4;
        
        assertTrue((a + b) == instance.add(a, b));
        
    }

    /**
     * Test of minus method, of class MyCalculator.
     */
    @Test
    public void testMinus() {
        System.out.println("minus");
        
        MyCalculator instance = new MyCalculator();
        
        float a = 3;
        float b = 4;
        
        assertTrue((a - b) == instance.minus(a, b));
    }

    /**
     * Test of divide method, of class MyCalculator.
     */
    @Test
    public void testDivide() {
        System.out.println("divide");
        
        MyCalculator instance = new MyCalculator();
        
        float a = 3;
        float b = 4;
        
        assertTrue((a / b) == instance.divide(a, b));
    }
    
    @Test(expected = ArithmeticException.class)
    public void testZeroDivision() {
        System.out.println("zero division");
        
        MyCalculator instance = new MyCalculator();
        
        float a = 3;
        float b = 0;
        
        instance.divide(a, b);
    }

    /**
     * Test of multiply method, of class MyCalculator.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        
        MyCalculator instance = new MyCalculator();
        
        float a = 3;
        float b = 4;
        
        assertTrue((a * b) == instance.multiply(a, b));
    }
    
}
